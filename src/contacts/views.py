from django.shortcuts import render
from django.http import HttpResponse

from .models import Contact

# Create your views here.
def index(request):
  all_contacts = Contact.objects.order_by('-updated')
  return render(request, 'contacts/index.html', {'contacts': all_contacts})

def detail(request):
  return HttpResponse('Contacts list')

