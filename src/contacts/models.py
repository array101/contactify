from django.db import models

class Contact(models.Model):
  name       = models.CharField(max_length=120)
  address    = models.CharField(max_length=120)
  phone      = models.CharField(max_length=120)
  email      = models.CharField(max_length=120)
  is_active  = models.BooleanField(default=True)
  timestamp  = models.DateTimeField(auto_now_add=True)
  updated    = models.DateTimeField(auto_now=True)

  def __str__(self):
    return self.name

class Tag(models.Model):
  contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
  name    = models.CharField(max_length=120)
